﻿using BingMapLatLongMigrator;
using Newtonsoft.Json;

namespace Mantle.CommandContracts
{
    public class RestaurantBaseCommand : BaseCommand
    {
        public string SalesForceCustomerId { get; set; }

        public int OriginalVersion { get; set; }

        public string[] ErrorMessages { get; set; }

        public bool Validated { get; set; }
    }
}