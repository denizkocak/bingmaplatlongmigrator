﻿using System;

namespace Mantle.CommandContracts
{
    public class BaseCommand
    {
        public Guid CorrelationId { get; set; }
    }
}