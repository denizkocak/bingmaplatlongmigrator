﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Mantle.CommandContracts;
using Newtonsoft.Json;

namespace BingMapLatLongMigrator
{
    class Program
    {
        private static string _logFileDir;

        static void Main(string[] args)
        {
            if (args?.Length == 0)
            {
                Console.WriteLine("Please enter the command arguments in order CsvFileLocation (Tap delimited), Mantle api url, Mantle API key, Log File Location");

                return;
            }

            var csvFileLocation = args[0];//Console.ReadLine(); //@"C:\users\deniz.kocak\desktop\rest.txt";

            var baseMantleUri = args[1];// Console.ReadLine(); //"http://mantle-api.dev.bookatable.internal/";

            var authenticationKey = args[2]; //Console.ReadLine(); //"18H9663731016838858U32FP3535Y843663BB5624QT1FPV729Y3V145836P";

            var whatIf = bool.Parse(args[3]);

            _logFileDir = args[4];//Console.ReadLine();// _logFileDir ?? @"C:\users\deniz.kocak\desktop\log.txt";

            using (var client = new HttpClient { BaseAddress = new Uri(baseMantleUri) })
            {
                using (var reader = new StreamReader(File.OpenRead(csvFileLocation)))
                {
                    reader.ReadLine();
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split('\t');

                        var restaurantId = values[0];

                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authenticationKey);
                        var result = client.GetAsync(string.Format("v1/restaurants/{0}", restaurantId)).Result;

                        if (result.StatusCode == HttpStatusCode.NotFound)
                        {
                            SaveLog($"Restaurant {restaurantId} does not exist. Skipping");

                            continue;
                        }

                        var resultString = result.Content.ReadAsStringAsync().Result;
                        var restaurantDetails = JsonConvert.DeserializeObject<RestaurantDetails>(resultString);

                        if (restaurantDetails.Location == null)
                        {
                            SaveLog($"Restaurant {restaurantId} does not have location property. Skipping");

                            continue;
                        }

                        var latlon = values[6].Substring(1).Substring(0, values[6].Length - 2).Split(',');

                        var latitude = latlon[0];
                        var longitude = latlon[1].Trim();

                        var correlationId = Guid.NewGuid();
                            
                        var geoCommand = new ChangeGeoLocationForRestaurant
                        {
                            MessageType = "Mantle.CommandContracts.ChangeGeoLocationForRestaurant, Mantle.CommandContracts",
                            CorrelationId = correlationId,
                            SalesForceCustomerId = restaurantId,
                            Latitude = restaurantDetails.Location.Latitude,
                            Longtitude = restaurantDetails.Location.Longtitude,
                            BingLatitude = double.Parse(latitude),
                            BingLongitude = double.Parse(longitude),
                            ErrorMessages = new List<string>().ToArray(),
                            OriginalVersion = restaurantDetails.VersionDetails.Version
                        };

                        var content = PreparePostContent(geoCommand);

                        if (!whatIf)
                        {
                            try
                            {
                                SaveLog($"Calling mantle api with {JsonConvert.SerializeObject(geoCommand)}");
                                var response = client
                                    .PostAsync(string.Format("v1/restaurants/{0}", restaurantId), content).Result;

                                if (response.IsSuccessStatusCode)
                                    SaveLog($"Restaurant {restaurantId} has been updated successfully.");
                                else
                                {
                                    SaveLog($"The response from the server is {response.StatusCode}. Body: {response.Content.ReadAsStringAsync().Result}");
                                }
                            }
                            catch (Exception e)
                            {
                                SaveLog(
                                    $"Error occured during calling restaurant {restaurantId}. Exception: {e}");
                            }
                        }
                        else
                        {
                            SaveLog($"I would have updated the restaurant {restaurantId}. With updated locations {latitude}/{longitude}");
                        }
                    }
                }
            }
        }

        private static void SaveLog(string log)
        {
            log = $"{DateTime.UtcNow:O} - {log}";
            Console.WriteLine(log);

            try
            {
                using (FileStream fs = new FileStream(_logFileDir, FileMode.Append, FileAccess.Write))
                using (var streamWriter = new StreamWriter(fs))
                {
                    streamWriter.WriteLineAsync(log).Wait();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        private static StringContent PreparePostContent(RestaurantBaseCommand command)
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None };
            var jsonCommand = JsonConvert.SerializeObject(command, typeof(RestaurantBaseCommand), settings);
            var content = new StringContent(jsonCommand, Encoding.UTF8, "application/json");
            content.Headers.Add("Source", "PartnerFeedService.BingMapLatLongMigrator");
            content.Headers.Add("Command", "Mantle.CommandContracts.ChangeGeoLocationForRestaurant, Mantle.CommandContracts");
            return content;
        }
    }
}
