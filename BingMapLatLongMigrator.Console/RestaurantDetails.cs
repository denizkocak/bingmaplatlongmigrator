namespace BingMapLatLongMigrator
{
    public class RestaurantDetails
    {
        public RestaurantLocation Location { get; set; }

        public RestaurantVersion VersionDetails { get; set; }
    }
}