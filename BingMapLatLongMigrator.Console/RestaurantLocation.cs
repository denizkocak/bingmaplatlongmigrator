﻿namespace BingMapLatLongMigrator
{
    public class RestaurantLocation
    {
        public double Longtitude { get; set; }

        public double Latitude { get; set; }
    }
}