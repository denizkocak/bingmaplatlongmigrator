﻿using BingMapLatLongMigrator;
using Newtonsoft.Json;

namespace Mantle.CommandContracts
{
    public class ChangeGeoLocationForRestaurant : RestaurantBaseCommand
    {
        [JsonProperty("$type")]
        public string MessageType { get; set; }

        public double Longtitude { get; set; }

        public double Latitude { get; set; }

        public bool FromImport { get; set; }

        public double BingLatitude { get; set; }

        public double BingLongitude { get; set; }
    }
}